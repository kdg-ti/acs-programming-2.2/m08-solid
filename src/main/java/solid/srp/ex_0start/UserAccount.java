package solid.srp.ex_0start;

public class UserAccount {
  private String name;
  private String phone;
  private String email;


  public UserAccount(String name , String email,String phone) {
    this.name = name;
    this.phone = phone;
    this.email = email;
  }

  public void changePhone(String phone) {
    if (checkAccess()) {
      this.phone=phone;
    }
  }

  public String getEmail() {
    return email;
  }

  public boolean checkAccess() {
    return getEmail().endsWith("kdg.be");
  }

  @Override
  public String toString() {
    return "UserAccount{" +
        "name='" + name + '\'' +
        ", phone='" + phone + '\'' +
        ", email='" + email + '\'' +
        '}';
  }
}
