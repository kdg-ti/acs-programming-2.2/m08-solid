package solid.openClosed.ex_0start;


import static solid.openClosed.ex_0start.Species.BEAR;
import static solid.openClosed.ex_0start.Species.COW;

/**
 * @author Jan de Rijke.
 */
public class Zoo {


	public static void main(String[] args) {
		Animal[] animals ={new Animal(COW),new Animal(BEAR)};
		for (Animal a : animals){
			System.out.println(a.getSpecies() +(a.sleeps()?" sleeps.":" is awake.") );
		}
	}
}