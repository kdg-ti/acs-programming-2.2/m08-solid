package solid.openClosed.ex_0start;


import solid.openClosed.ZooTime;

/**
 * @author Jan de Rijke.
 */
public class Animal {
	private Species species;



	public Animal(Species species) {
		this.species = species;
	}

	public Species getSpecies() {
		return species;
	}

	public boolean sleeps() {
		ZooTime date = new ZooTime();
		return switch (species) {
			case COW ->  date.isNight();
			case BEAR -> date.isWinter();
		};
	}
}
