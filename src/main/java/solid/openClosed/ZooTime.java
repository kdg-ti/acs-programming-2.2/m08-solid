package solid.openClosed;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class ZooTime {
	LocalDate date ;

	public ZooTime(LocalDate date) {
		this.date = date;
	}

	public ZooTime() {
		date=LocalDate.now();
	}

	public boolean isNight(){
		int hour = LocalDateTime.now().getHour();
		return hour > 22 || hour < 6;
	}

	public boolean isWinter(){
		return switch (LocalDate.now().getMonth()) {
			case DECEMBER, JANUARY, FEBRUARY -> true;
			default -> false;
		};
	}
}
