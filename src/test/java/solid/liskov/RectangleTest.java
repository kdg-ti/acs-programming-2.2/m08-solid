package solid.liskov;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RectangleTest {

	@Test
	void areaShouldBeWidthXHeight() {
		Rectangle r = new Square(4);
		r.setWidth(5);
		r.setHeight(10);
		assertEquals(50,r.getArea());
	}
}